package com.seop.medicalmanager.service;

import com.seop.medicalmanager.entity.Customer;
import com.seop.medicalmanager.model.CustomerRequest;
import com.seop.medicalmanager.repository.CustomerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
public class CustomerService {
    private final CustomerRepository customerRepository;

    public void setCustomer(CustomerRequest request) {
        Customer addData = new Customer.CustomerBuilder(request).build();

        customerRepository.save(addData);
    }

    public Customer getCustomerId(long id) {  //아이디를 봐꿔먹기 위한 매서드
        return customerRepository.findById(id).orElseThrow();
    }
}
