package com.seop.medicalmanager.service;

import com.seop.medicalmanager.entity.ClinicHistory;
import com.seop.medicalmanager.entity.Customer;
import com.seop.medicalmanager.enums.MedicalItems;
import com.seop.medicalmanager.model.CustomerResponse;
import com.seop.medicalmanager.model.HistoryItem;
import com.seop.medicalmanager.model.HistoryRequest;
import com.seop.medicalmanager.model.MedicalCorporationItem;
import com.seop.medicalmanager.repository.ClinicHistoryRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class HistoryService {
    private final ClinicHistoryRepository clinicHistoryRepository;

    public void setHistoryCustomer(Customer customer, HistoryRequest request) { //customer 테이블에 있는 정보를 가져오기위함
        ClinicHistory addData = new ClinicHistory.ClinicHistoryBuilder(customer, request).build();

        clinicHistoryRepository.save(addData);
    }
    public List<HistoryItem> getHistoriesByDate(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByDateCureOrderByIdDesc(searchDate);
        List<HistoryItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            HistoryItem addItem = new HistoryItem();
            addItem.setHistoryId(item.getId());
            addItem.setCustomerId(item.getCustomer().getId());
            addItem.setCustomerName(item.getCustomer().getCustomerName());
            addItem.setCustomerPhone(item.getCustomer().getCustomerPhone());
            addItem.setCustomerRegistration(item.getCustomer().getRegistrationNumber());
            addItem.setMedicalItemName(item.getMedicalItems().getName());
            addItem.setPrice(item.getPrice());
            addItem.setIsSalaryName(item.getIsSalary() ? "예" : "아니요");
            addItem.setDateCure(item.getDateCure());
            addItem.setTimeCure(item.getTimeCure());
            addItem.setIsCalculateName(item.getIsCalculate() ? "예" : "아니요");

            result.add(addItem);
        }
        return result;
    }

    public List<HistoryItem> getHistoriesByMedicalItem(MedicalItems searchData) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByMedicalItemsOrderByIdAsc(searchData);
        List<HistoryItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            HistoryItem addItem = new HistoryItem();
            addItem.setHistoryId(item.getId());
            addItem.setMedicalItemName(item.getMedicalItems().getName());

            result.add(addItem);
        }
        return result;
    }

    public List<MedicalCorporationItem> getMedicalCorporation(LocalDate searchDate) {
        List<ClinicHistory> originList = clinicHistoryRepository.findAllByIsSalaryAndDateCureAndIsCalculateOrderByIdDesc(true, searchDate, true);

        List<MedicalCorporationItem> result = new LinkedList<>();

        for (ClinicHistory item : originList) {
            MedicalCorporationItem addItem = new MedicalCorporationItem();
            addItem.setCustomerName(item.getCustomer().getCustomerName());
            addItem.setCustomerPhone(item.getCustomer().getCustomerPhone());
            addItem.setCustomerRegistration(item.getCustomer().getRegistrationNumber());
            addItem.setMedicalItem(item.getMedicalItems().getName());
            addItem.setNonSalaryPrice(item.getMedicalItems().getNonSalaryPrice());
            addItem.setPrice(item.getPrice());
            addItem.setClaimsPrice(item.getMedicalItems().getNonSalaryPrice() - item.getPrice());

            result.add(addItem);
        }
        return result;
    }

    public CustomerResponse getCustomerInfo(long id) {
        ClinicHistory originData = clinicHistoryRepository.findById(id).orElseThrow();

        CustomerResponse result = new CustomerResponse();

        result.setId(originData.getCustomer().getId());
        result.setHistoryId(originData.getId());
        result.setCustomerNamePhone(originData.getCustomer().getCustomerName() + "/" + originData.getCustomer().getCustomerPhone());
        result.setCustomerAddress(originData.getCustomer().getAddress());
        result.setRegistrationNumber(originData.getCustomer().getRegistrationNumber());
        result.setMemo(originData.getCustomer().getMemo());
        result.setVisitFirst(originData.getCustomer().getDateFirst());
        result.setMedicalItemName(originData.getMedicalItems().getName());
        result.setNonSalaryPrice(originData.getMedicalItems().getNonSalaryPrice() + "원");
        result.setPrice(originData.getPrice() + "원");
        result.setClaimsPrice((originData.getMedicalItems().getNonSalaryPrice() - originData.getPrice()) + "원");
        result.setIsSalaryName(originData.getIsSalary() ? "예" : "아니요");
        result.setIsCalculateName(originData.getIsCalculate() ? "예" : "아니요");
        result.setDateTimeCure(originData.getDateCure() + "/" + originData.getTimeCure());

        return result;
    }
}
