package com.seop.medicalmanager.repository;

import com.seop.medicalmanager.entity.ClinicHistory;
import com.seop.medicalmanager.enums.MedicalItems;
import org.springframework.data.jpa.repository.JpaRepository;

import java.time.LocalDate;
import java.util.List;

public interface ClinicHistoryRepository extends JpaRepository<ClinicHistory, Long> {
    List<ClinicHistory> findAllByDateCureOrderByIdDesc(LocalDate dateCure);

    List<ClinicHistory> findAllByMedicalItemsOrderByIdAsc(MedicalItems medicalItems);

    List<ClinicHistory> findAllByIsSalaryAndDateCureAndIsCalculateOrderByIdDesc(
            boolean isSalary, LocalDate dateCure, boolean isCalculate);
}
