package com.seop.medicalmanager;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedicalManagerApplication {

    public static void main(String[] args) {
        SpringApplication.run(MedicalManagerApplication.class, args);
    }

}
