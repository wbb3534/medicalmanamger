package com.seop.medicalmanager.entity;

import com.seop.medicalmanager.enums.MedicalItems;
import com.seop.medicalmanager.interfaces.CommonModelBuilder;
import com.seop.medicalmanager.model.HistoryRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class ClinicHistory {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne(fetch = FetchType.LAZY)                  // LAZY 안하면 필요하지않을때도 데이터가 나타남
    @JoinColumn(name = "customerId", nullable = false)  //customer테이블에서 id를 가져오기 위함
    private Customer customer;
    @Column(nullable = false, length = 20)
    @Enumerated(value = EnumType.STRING)
    private MedicalItems medicalItems;
    @Column(nullable = false)
    private Double price;
    @Column(nullable = false)
    private Boolean isSalary;
    @Column(nullable = false)
    private LocalDate dateCure;
    @Column(nullable = false)
    private LocalTime timeCure;
    @Column(nullable = false)
    private Boolean isCalculate;

    private ClinicHistory(ClinicHistoryBuilder builder) {
        this.customer = builder.customer;
        this.medicalItems = builder.medicalItems;
        this.price = builder.price;
        this.isSalary = builder.isSalary;
        this.dateCure = builder.dateCure;
        this.timeCure = builder.timeCure;
        this.isCalculate = builder.isCalculate;
    }

    public static class ClinicHistoryBuilder implements CommonModelBuilder<ClinicHistory> {
        private final Customer customer;
        private final MedicalItems medicalItems;
        private final Double price;
        private final Boolean isSalary;
        private final LocalDate dateCure;
        private final LocalTime timeCure;
        private final Boolean isCalculate;

        public ClinicHistoryBuilder(Customer customer, HistoryRequest request) {
            this.customer = customer;
            this.medicalItems = request.getMedicalItems();
            this.price = request.getIsSalary() ? request.getMedicalItems().getSalaryPrice() : request.getMedicalItems().getNonSalaryPrice();
            this.isSalary = request.getIsSalary();
            this.dateCure = LocalDate.now();
            this.timeCure = LocalTime.now();
            this.isCalculate = false;
        }

        @Override
        public ClinicHistory build() {
            return new ClinicHistory(this);
        }
    }
}
