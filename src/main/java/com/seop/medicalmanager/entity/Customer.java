package com.seop.medicalmanager.entity;

import com.seop.medicalmanager.interfaces.CommonModelBuilder;
import com.seop.medicalmanager.model.CustomerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED) //아무도 못들어오게 한다/
public class Customer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(nullable = false, length = 20)
    private String customerName;
    @Column(nullable = false, length = 13)
    private String customerPhone;
    @Column(nullable = false, length = 14)
    private String registrationNumber;
    @Column(nullable = false, length = 20)
    private String address;
    @Column(nullable = false, columnDefinition = "TEXT") //글자수 무제한
    private String memo;
    @Column(nullable = false)
    private LocalDate dateFirst;

    private Customer(CustomerBuilder builder) {
        this.customerName = builder.customerName;
        this.customerPhone = builder.customerPhone;
        this.registrationNumber = builder.registrationNumber;
        this.address = builder.address;
        this.memo = builder.memo;
        this.dateFirst = builder.dateFirst;
    }

    public static class CustomerBuilder implements CommonModelBuilder<Customer> {
        private final String customerName;
        private final String customerPhone;
        private final String registrationNumber;
        private final String address;
        private final String memo;
        private final LocalDate dateFirst;

        public CustomerBuilder(CustomerRequest request) {
            this.customerName = request.getCustomerName();
            this.customerPhone = request.getCustomerPhone();
            this.registrationNumber = request.getRegistrationNumber();
            this.address = request.getAddress();
            this.memo = request.getMemo();
            this.dateFirst = LocalDate.now();
        }

        @Override
        public Customer build() {
            return new Customer(this);
        }
    }
}
