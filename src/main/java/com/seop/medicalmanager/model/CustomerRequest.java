package com.seop.medicalmanager.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CustomerRequest {
    @NotNull
    @Length(min = 2, max = 20)
    private String customerName;
    @NotNull
    @Length(min = 13, max = 13)
    private String customerPhone;
    @NotNull
    @Length(min = 9, max = 14)
    private String registrationNumber;
    @NotNull
    @Length(min = 5, max = 200)
    private String address;
    @NotNull
    private String memo;
}
