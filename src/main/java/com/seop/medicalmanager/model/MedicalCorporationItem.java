package com.seop.medicalmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

@Getter
@Setter
public class MedicalCorporationItem {
    private String customerName;
    private String customerPhone;
    private String customerRegistration;
    private String medicalItem;
    private Double nonSalaryPrice;
    private Double price;
    private Double claimsPrice;
}
