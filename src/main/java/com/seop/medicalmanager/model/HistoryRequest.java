package com.seop.medicalmanager.model;

import com.seop.medicalmanager.enums.MedicalItems;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.validation.constraints.NotNull;

@Getter
@Setter
public class HistoryRequest {
    @NotNull
    @Enumerated(value = EnumType.STRING)
    private MedicalItems medicalItems;
    @NotNull
    private Boolean isSalary;
}
