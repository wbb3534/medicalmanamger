package com.seop.medicalmanager.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
public class CustomerResponse {
    private Long id;
    private Long historyId;
    private String customerNamePhone;
    private String registrationNumber;
    private String customerAddress;
    private String memo;
    private LocalDate visitFirst;
    private String medicalItemName;
    private String price;     // 가격
    private String nonSalaryPrice; // 비공제금액
    private String claimsPrice; //의료공단 청구금액
    private String dateTimeCure;
    private String isSalaryName;
    private String isCalculateName;
}
