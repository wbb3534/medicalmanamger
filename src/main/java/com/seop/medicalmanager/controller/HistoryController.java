package com.seop.medicalmanager.controller;

import com.seop.medicalmanager.entity.Customer;
import com.seop.medicalmanager.enums.MedicalItems;
import com.seop.medicalmanager.model.CustomerResponse;
import com.seop.medicalmanager.model.HistoryItem;
import com.seop.medicalmanager.model.HistoryRequest;
import com.seop.medicalmanager.model.MedicalCorporationItem;
import com.seop.medicalmanager.service.CustomerService;
import com.seop.medicalmanager.service.HistoryService;
import lombok.RequiredArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/history")
public class HistoryController {
    private final CustomerService customerService;
    private final HistoryService historyService;

    @PostMapping("/new/customer/{customerId}")
    public String setHistory(@PathVariable long customerId, @RequestBody @Valid HistoryRequest request) {
        Customer customer = customerService.getCustomerId(customerId);
        historyService.setHistoryCustomer(customer, request);

        return "ok";
    }

    @GetMapping("/all/date") //파람 웹사이트 ? 뒤에 키 벨류로 되어있는 애들
    public List<HistoryItem> getHistoriesByDate(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        List<HistoryItem> result = historyService.getHistoriesByDate(searchDate);

        return result;
    }

    @GetMapping("/all/item")
    public List<HistoryItem> getHistoriesByItem(@RequestParam("searchData") MedicalItems searchData) {
        List<HistoryItem> result = historyService.getHistoriesByMedicalItem(searchData);

        return result;
    }

    @GetMapping("/all/insurance")
    public List<MedicalCorporationItem> getMedicalCorporation(@RequestParam("searchDate") @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate searchDate) {
        return historyService.getMedicalCorporation(searchDate);
    }

    @GetMapping("/detail/id/{id}")
    public CustomerResponse getCustomerInfo(@PathVariable long id) {
        return historyService.getCustomerInfo(id);
    }
}
