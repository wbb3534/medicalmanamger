package com.seop.medicalmanager.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
